Disabled User-Interface
--------------------------------------------------------------------------
For security reasons, the Debian package has disabled the user-interface
by default, as having the interface enabled, would allow anyone in the
same network as your gerbera daemon to access your filesystem (as user
'gerbera') without any authentication.
Note that the 'gerbera' user does not have excessive permissions (but
arguably more than any random person who can connect to your network should
have).
For a discussion of this issue, see Debian bugs #580120 & #778669.
To enable the user-interface, edit /etc/gerbera/config.xml and change the
line containing
    <ui enabled="no" ...
to
    <ui enabled="yes" ...
If you do that, you should make sure to protect your data otherwise (e.g. by
enabling user accounts, making sure that the daemon only listens on 127.0.0.1
and so on).
--------------------------------------------------------------------------
